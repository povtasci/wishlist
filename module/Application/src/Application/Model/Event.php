<?php
namespace Application\Model;

use Auth\Model\Auth;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Event implements InputFilterAwareInterface
{
    protected $inputFilter;
    protected $event_id;
    protected $event_user_id;
    protected $event_type;
    protected $event_name;
    protected $event_date;
    protected $event_status;
    protected $event_created_at;
    protected $user;

    public function __construct(array $data = array())
    {
        $this->exchangeArray($data);
    }

    public function exchangeArray(array $data)
    {
        foreach ($data as $k => $v) {
            if (property_exists($this, $k)) {
                $this->$k = $v;
            }
        }
    }

    public function getArrayCopy()
    {
        $arr = array(
            'event_user_id' => $this->getUserId(),
            'event_name' => $this->getName(),
            'event_type' => $this->getType(),
            'event_date' => $this->getDate(),
            'event_status' => $this->getStatus(),
            'event_created_at' => $this->getCreatedAt(),
        );
        if ($this->getId()) {
            $arr['event_id'] = $this->getId();
        }

        return $arr;
    }

    public function getArrayCopyForInsert()
    {
        $arr = $this->getArrayCopy();
        if (array_key_exists("event_id", $arr)) unset($arr['event_id']);
        if (array_key_exists("event_created_at", $arr)) unset($arr['event_created_at']);

        return $arr;
    }

    public function getId()
    {
        return $this->event_id;
    }

    public function setId($event_id)
    {
        $this->event_id = $event_id;
        return $this->event_id;
    }

    public function getUserId()
    {
        return $this->event_user_id;
    }

    public function setUserId($event_user_id)
    {
        $this->event_user_id = $event_user_id;
        return $this->event_user_id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(Auth $user)
    {
        $this->user = $user;
        return $this->user;
    }

    public function getName()
    {
        return $this->event_name;
    }

    public function setName($event_name)
    {
        $this->event_name = $event_name;
        return $this;
    }

    public function getType()
    {
        return $this->event_type;
    }

    public function setType($event_type)
    {
        $this->event_type = $event_type;
        return $this;
    }

    public function getDate($format = false)
    {
        if ($format) {
            return date("j M. Y", strtotime($this->event_date));
        }
        return $this->event_date;
    }

    public function setDate($event_date)
    {
        $this->event_date = $event_date;
        return $this;
    }

    public function getStatus()
    {
        return $this->event_status;
    }

    public function setStatus($event_status)
    {
        $this->event_status = $event_status;
        return $this;
    }

    public function getCreatedAt($format = false)
    {
        if ($format) {
            return date("F j. Y", strtotime($this->event_created_at));
        }
        return $this->event_created_at;
    }

    public function getDay()
    {
        return date("d", strtotime($this->getDate()));
    }

    public function getMonth()
    {
        return date("m", strtotime($this->getDate()));
    }

    public function getYear()
    {
        return date("Y", strtotime($this->getDate()));
    }

    public function setCreatedAt($event_created_at)
    {
        $this->event_created_at = $event_created_at;
        return $this;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name' => 'event_type',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Type is required",
                            ),
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'event_name',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'event_date',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Date is required",
                            ),
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'event_status',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}