<?php
namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Application\Model\Event;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class EventTable
{
    protected $tableGateway;
    protected $adapter;
    protected $_primary = "event_id";

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter = new Adapter($this->tableGateway->getAdapter()->getDriver());
    }

    public function fetchAll($where = array(), $page = false, $limit = 10)
    {
        $whereString = "";
        if ($where) {
            foreach ($where as $key => $val) {
                $whereString .= ($whereString ? " AND " : "WHERE ") . "`{$key}` = '{$val}'";
            }
        }
        $limitString = "";
        if ($page) {
            $limitString = " LIMIT " . (($page - 1) * $limit) . ", " . $limit;
        }

        $resultSet = $this->adapter->query("SELECT events.* FROM events
                                                $whereString ORDER BY event_id DESC $limitString")->execute();
        if ($resultSet->count() == 0) {
            return false;
        }

        $products = array();
        while ($row = $resultSet->current()) {
            $products[$row['event_id']] = new Event($row);
            $resultSet->next();
        }


        $resultSet = $this->adapter->query("SELECT count(*) AS c FROM events $whereString")->execute();
        $row = $resultSet->current();
        return array(
            "list" => $products,
            "count" => $row['c'],
        );
    }

    public function findOne($where = array())
    {
        $whereString = "";
        if ($where) {
            foreach ($where as $key => $val) {
                $whereString .= ($whereString ? " AND " : "WHERE ") . "`{$key}` = '{$val}'";
            }
        }

        $resultSet = $this->adapter->query("SELECT events.* FROM events $whereString")->execute();
        if ($resultSet->count() == 0) {
            return false;
        }

        $event =  new Event($resultSet->current());

        return $event;
    }

    public function save(Event $obj)
    {
        $data = $obj->getArrayCopyForInsert();
        $id = (int) $obj->getId();
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $res = $this->find($this->tableGateway->lastInsertValue);
        } else {
            if ($this->find($id)) {
                $this->tableGateway->update($data, array($this->_primary => $id));
                $res = $obj;
            } else {
                throw new \Exception('Form id does not exist');
            }
        }

        return $res;
    }

    public function createNew()
    {
        return new Event();
    }

    public function find($id) {
        $resultSet = $this->adapter->query("SELECT events.* FROM events
                                                WHERE events.event_id = '{$id}'")->execute();
        if ($resultSet->count() == 0) {
            return false;
        }

        $product = new Event();
        while ($row = $resultSet->current()) {
            if (!$product->getId()) {
                $product->exchangeArray($row);

            }
            $resultSet->next();
        }

        return $product;
    }

    public function delete($id)
    {
        $this->tableGateway->delete(array("event_id" => $id));
        return true;
    }
}