<?php
namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Application\Model\Product;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class ProductTable
{
    protected $tableGateway;
    protected $adapter;
    protected $_primary = "product_id";

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter = new Adapter($this->tableGateway->getAdapter()->getDriver());
    }

    public function fetchAll($where = array(), $page = false, $limit = 10)
    {
        $whereString = "";
        if ($where) {
            foreach ($where as $key => $val) {
                $whereString .= ($whereString ? " AND " : "WHERE ") . "`{$key}` = '{$val}'";
            }
        }
        $limitString = "";
        if ($page) {
            $limitString = " LIMIT " . (($page - 1) * $limit) . ", " . $limit;
        }

        $resultSet = $this->adapter->query("SELECT products.* FROM products
                                                $whereString ORDER BY product_id DESC $limitString")->execute();
        if ($resultSet->count() == 0) {
            return false;
        }

        $products = array();
        while ($row = $resultSet->current()) {
            $products[$row['product_id']] = new Product($row);
            $resultSet->next();
        }


        $resultSet = $this->adapter->query("SELECT count(*) AS c FROM products $whereString")->execute();
        $row = $resultSet->current();
        return array(
            "list" => $products,
            "count" => $row['c'],
        );
    }

    public function save(Product $obj)
    {
        $data = $obj->getArrayCopyForInsert();
        $id = (int) $obj->getId();
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $res = $this->find($this->tableGateway->lastInsertValue);
        } else {
            if ($this->find($id)) {
                $this->tableGateway->update($data, array($this->_primary => $id));
                $res = $obj;
            } else {
                throw new \Exception('Form id does not exist');
            }
        }

        return $res;
    }

    public function createNew()
    {
        return new Product();
    }

    public function addGenreToBook($genreId, $bookId)
    {
        $this->adapter->query("INSERT IGNORE INTO rel_book_genre SET `rbg_genre_id` = '{$genreId}', `rbg_book_id` = '{$bookId}'")->execute();
        return true;
    }

    public function addTest($title)
    {
        $this->adapter->query("INSERT INTO products SET `product_title` = '{$title}'")->execute();
        return true;
    }

    public function removeGenresFromBook($bookId)
    {
        $this->adapter->query("DELETE FROM rel_book_genre WHERE `rbg_book_id` = '{$bookId}'")->execute();
        return true;
    }

    public function find($id) {
        $resultSet = $this->adapter->query("SELECT products.* FROM products
                                                WHERE products.product_id = '{$id}'")->execute();
        if ($resultSet->count() == 0) {
            return false;
        }

        $product = new Product();
        while ($row = $resultSet->current()) {
            if (!$product->getId()) {
                $product->exchangeArray($row);

            }
            $resultSet->next();
        }

        return $product;
    }

    public function delete($id)
    {
        $this->tableGateway->delete(array($this->_primary => $id));
        return true;
    }

    public function getGenresByBookId($bookId)
    {
        $resultSet = $this->adapter->query("SELECT genres.* FROM rel_book_genre AS rel
                                                LEFT JOIN genres ON genres.genre_id = rel.rbg_genre_id
                                                WHERE rel.rbg_book_id = '{$bookId}'")->execute();
        if ($resultSet->count() == 0) {
            return false;
        }
        $genres = array();
        while ($row = $resultSet->current()) {
            $genres[] = new Genre($row);
            $resultSet->next();
        }

        return $genres;
    }

}