<?php
namespace Application\Model;

use Auth\Model\Auth;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Product
{
    protected $inputFilter;
    protected $product_id;
    protected $product_user_id;
    protected $product_image;
    protected $product_shop;
    protected $product_url;
    protected $product_name;
    protected $product_price;
    protected $product_status;
    protected $product_created_at;
    protected $user;

    public function __construct(array $data = array())
    {
        $this->exchangeArray($data);
    }

    public function exchangeArray(array $data)
    {
        foreach ($data as $k => $v) {
            if (property_exists($this, $k)) {
                $this->$k = $v;
            }
        }
    }

    public function getArrayCopy()
    {
        $arr = array(
            'product_name' => $this->getName(),
            'product_user_id' => $this->getUserId(),
            'product_image' => $this->getImage(),
            'product_url' => $this->getUrl(),
            'product_shop' => $this->getShop(),
            'product_price' => $this->getPrice(),
            'product_status' => $this->getStatus(),
            'product_created_at' => $this->getCreatedAt(),
        );
        if ($this->getId()) {
            $arr['product_id'] = $this->getId();
        }

        return $arr;
    }

    public function getArrayCopyForInsert()
    {
        $arr = $this->getArrayCopy();
        if (array_key_exists("product_id", $arr)) unset($arr['product_id']);
        if (array_key_exists("product_created_at", $arr)) unset($arr['product_created_at']);

        return $arr;
    }

    public function getId()
    {
        return $this->product_id;
    }

    public function setId($product_id)
    {
        $this->product_id = $product_id;
        return $this->product_id;
    }

    public function getUserId()
    {
        return $this->product_user_id;
    }

    public function setUserId($product_user_id)
    {
        $this->product_user_id = $product_user_id;
        return $this->product_user_id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(Auth $user)
    {
        $this->user = $user;
        return $this->user;
    }

    public function getName()
    {
        return $this->product_name;
    }

    public function setName($product_name)
    {
        $this->product_name = $product_name;
        return $this;
    }

    public function getImage()
    {
        return $this->product_image;
    }

    public function setImage($product_image)
    {
        $this->product_image = $product_image;
        return $this;
    }

    public function getUrl()
    {
        return $this->product_url;
    }

    public function setUrl($product_url)
    {
        $this->product_url = $product_url;
        return $this;
    }

    public function getShop()
    {
        return $this->product_shop;
    }

    public function setShop($product_shop)
    {
        $this->product_shop = $product_shop;
        return $this;
    }

    public function getPrice()
    {
        return $this->product_price;
    }

    public function setPrice($product_price)
    {
        $this->product_price = $product_price;
        return $this;
    }

    public function getStatus()
    {
        return $this->product_status;
    }

    public function setStatus($product_status)
    {
        $this->product_status = $product_status;
        return $this;
    }

    public function getCreatedAt($format = false)
    {
        if ($format) {
            return date("F j, Y", strtotime($this->product_created_at));
        }
        return $this->product_created_at;
    }

    public function setCreatedAt($product_created_at)
    {
        $this->product_created_at = $product_created_at;
        return $this;
    }

}