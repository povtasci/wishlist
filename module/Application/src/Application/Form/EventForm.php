<?php
namespace Application\Form;

use Zend\Form\Form;

class EventForm extends Form
{
    public function __construct($name=null)
    {
        parent::__construct('event');
        $this->setAttribute('method','post');

        $this->add(array(
            'name' => 'event_type',
            'attributes' => array(
                'type'  => 'text',
            ),
        ));

        $this->add(array(
            'name' => 'event_name',
            'attributes' => array(
                'type'  => 'text',
            ),
        ));

        $this->add(array(
            'name' => 'event_date',
            'attributes' => array(
                'type'  => 'text',
            ),
        ));

        $this->add(array(
            'name' => 'event_status',
            'attributes' => array(
                'type'  => 'text',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Login',
                'id' => 'submit',
                'class'=>'btn btn-success'
            ),
        ));

    }
}