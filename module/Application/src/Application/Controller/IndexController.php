<?php
namespace Application\Controller;

use Zend\Dom\Document;
use Zend\View\Model\ViewModel;
//use Zend\Dom\Document\Query;
use Zend\Http\Client;
use Zend\Dom\Query;

class IndexController extends RootController
{
    public function indexAction()
    {
        if (!$this->getUser()->getId()) {
            return $this->redirect()->toUrl("/login");
        }

        $products = $this->getProductTable()->fetchAll(array("product_user_id" => $this->getUser()->getId(), "product_status" => "active"));

        $events = $this->getEventTable()->fetchAll(array("event_user_id" => $this->getUser()->getId(), "event_status" => "public"));
        $events = $events ? $events['list'] : false;
        $myBirthdayEvent = false;
        if ($events) {
            foreach ($events as $key => $event) {
                if ($event->getType() == "My Birthday") {
                    $myBirthdayEvent = $event;
                    unset($events[$key]);
                    break;
                }
            }
        }
        return new ViewModel(array(
            "products" => $products['list'],
            "theUser" => $this->getUser(),
            "events" => $events,
            "myBirthdayEvent" => $myBirthdayEvent,
        ));
    }

    public function wishListAction()
    {
        if (!($id = $this->params()->fromRoute("id", false)) || !($user = $this->getAuthTable()->fetchById($id))) {
            return $this->redirect()->toUrl("/");
        }

        $products = $this->getProductTable()->fetchAll(array("product_user_id" => $user->getId(), "product_status" => "active"));
        $events = $this->getEventTable()->fetchAll(array("event_user_id" => $user->getId(), "event_status" => "public"));
        $events = $events ? $events['list'] : false;
        $myBirthdayEvent = false;
        if ($events) {
            foreach ($events as $key => $event) {
                if ($event->getType() == "My Birthday") {
                    $myBirthdayEvent = $event;
                    unset($events[$key]);
                    break;
                }
            }
        }
        return new ViewModel(array(
            "products" => $products['list'],
            "theUser" => $user,
            "events" => $events,
            "myBirthdayEvent" => $myBirthdayEvent,
            "page" => 'wishList',
        ));
    }

    public function archiveAction()
    {
        if (!$this->getUser()->getId()) {
            return $this->redirect()->toUrl("/");
        }

        $products = $this->getProductTable()->fetchAll(array("product_user_id" => $this->getUser()->getId(), "product_status" => "archive"));
        $events = $this->getEventTable()->fetchAll(array("event_user_id" => $this->getUser()->getId(), "event_status" => "public"));
        $events = $events ? $events['list'] : false;

        $myBirthdayEvent = false;
        if ($events) {
            foreach ($events as $key => $event) {
                if ($event->getType() == "My Birthday") {
                    $myBirthdayEvent = $event;
                    unset($events[$key]);
                    break;
                }
            }
        }
        return new ViewModel(array(
            "products" => $products['list'],
            "theUser" => $this->getUser(),
            "events" => $events,
            "myBirthdayEvent" => $myBirthdayEvent,
        ));
    }

    public function searchAction()
    {
        $users = false;
        if ($q = $this->params()->fromQuery("q", false)) {
            $users = $this->getAuthTable()->search($q);
        }

        return new ViewModel(array(
            "users" => $users,
        ));
    }

    public function loginAction()
    {
        if ($this->getUser()->getId()) {
            return $this->redirect()->toUrl("/");
        }

        if ($back = $this->params()->fromQuery("back", false)) {
            $this->setLastUrl(urldecode($back));
        }
        $layout = $this->layout();
        $layout->setTemplate('layout/login');

        return new ViewModel(array(
        ));
    }

    public function signUpAction()
    {
        if ($this->getUser()->getId()) {
            return $this->redirect()->toUrl("/");
        }

        $layout = $this->layout();
        $layout->setTemplate('layout/login');

        return new ViewModel(array(
        ));
    }
}
