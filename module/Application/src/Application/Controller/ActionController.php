<?php
namespace Application\Controller;

use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Dom\Document;
use Zend\Http\Client;
use Zend\Dom\Query;

class ActionController extends RootController
{
    public function moveToArchiveAction()
    {
        if (!$this->getUser()->getId()) {
            return new JsonModel(array(
                "returnCode" => 203,
                "message" => "You need to log in.",
            ));
        }

        if (!($id = $this->params()->fromRoute("id", false)) || !($product = $this->getProductTable()->find($id))) {
            return new JsonModel(array(
                "returnCode" => 201,
                "message" => "Wrong parameters.",
            ));
        }

        $product->setStatus("archive");
        $this->getProductTable()->save($product);

        return new JsonModel(array(
            "returnCode" => 101,
            "message" => "The item has been moved to archive.",
        ));
    }
    public function moveToListAction()
    {
        if (!$this->getUser()->getId()) {
            return new JsonModel(array(
                "returnCode" => 203,
                "message" => "You need to log in.",
            ));
        }

        if (!($id = $this->params()->fromRoute("id", false)) || !($product = $this->getProductTable()->find($id))) {
            return new JsonModel(array(
                "returnCode" => 201,
                "message" => "Wrong parameters.",
            ));
        }

        $product->setStatus("active");
        $this->getProductTable()->save($product);

        return new JsonModel(array(
            "returnCode" => 101,
            "message" => "The item has been moved to your list.",
        ));
    }
    public function deleteProductAction()
    {
        if (!$this->getUser()->getId()) {
            return new JsonModel(array(
                "returnCode" => 203,
                "message" => "You need to log in.",
            ));
        }

        if (!($id = $this->params()->fromRoute("id", false)) || !($product = $this->getProductTable()->find($id))) {
            return new JsonModel(array(
                "returnCode" => 201,
                "message" => "Wrong parameters.",
            ));
        }

        $this->getProductTable()->delete($product->getId());

        return new JsonModel(array(
            "returnCode" => 101,
            "message" => "The item has been moved to archive.",
        ));
    }

    public function addManualLinkAction()
    {
        return $this->grabAction();
    }

    public function grabAction()
    {
        if (!$this->getUser()->getId()) {
            return new JsonModel(array(
                "returnCode" => 203,
                "result" => array(
                    "url" => $this->getConfig()->site['domain'] . "/login",
                ),
            ));
        }
        $url = $this->params()->fromPost("url", false);
//        $url = $this->params()->fromPost("url", "http://www.amazon.com/gp/product/0062423355/ref=s9_simh_gw_p14_d0_i4?pf_rd_m=ATVPDKIKX0DER&pf_rd_s=desktop-2&pf_rd_r=1ZSD133ATRE42JBDFVNN&pf_rd_t=36701&pf_rd_p=2091268722&pf_rd_i=desktop");
        if (!$url) {
            return new JsonModel(array(
                'returnCode' => 201,
                'result' => array(
                    "showMessage" => true,
                ),
                "message" => $this->getErrorMsgZendFormat("Wrong url. There is a problem with url or now we don't support this site.")
            ));
        }

        $urlParse = parse_url($url);
        $info = array();
        if (!array_key_exists('host', $urlParse)) {
            return new JsonModel(array(
                'returnCode' => 201,
                'result' => array(
                    "showMessage" => true,
                ),
                "message" => $this->getErrorMsgZendFormat("Sorry! Having trouble to save the link?<br>Add new website to Presont. <a href='javascript:void(0)' data-toggle='modal' data-target='#HelpModalBox' data-dismiss='modal'>Click here</a>.")
            ));
        }
        if ($urlParse['host'] == "www.vertbaudet.ch" || $urlParse['host'] == "vertbaudet.ch") {
            $client = new Client($url);
            $response = $client->send();

            $query = new Query($response->getBody());
            $head = $query->execute("html");
            $elements = $head->current()->getElementsByTagName("meta");
            $info = array();
            foreach ($elements as $a) {
                $obj = $a->getAttributeNode('property');
                if ($obj) {
                    if ($obj->nodeValue == "og:url") {
                        $info["product_url"] = $a->getAttributeNode('content')->nodeValue;
                    } elseif ($obj->nodeValue == "og:title") {
                        $info["product_name"] = $a->getAttributeNode('content')->nodeValue;
                    } elseif ($obj->nodeValue == "og:image") {
                        $info["product_image"] = $a->getAttributeNode('content')->nodeValue;
                    } elseif ($obj->nodeValue == "og:site_name") {
                        $info["product_shop"] = $a->getAttributeNode('content')->nodeValue;
                    }
                }
            }

            $head = $query->execute(".memo_fp_prix_full");

            $elements = $head->current()->getElementsByTagName("span");
            foreach ($elements as $a) {
                $obj = $a->getAttributeNode('class');
                if ($obj) {
                    if ($obj->nodeValue == "memo_fp_prix_final") {
                        $info["product_price"] = "CHF " . $a->getAttributeNode('data-price')->nodeValue;
                    }
                }
            }

        } elseif ($urlParse['host'] == "www.mylittleroom.ch" || $urlParse['host'] == "mylittleroom.ch") {
            $client = new Client($url);
            $response = $client->send();

            $query = new Query($response->getBody());


            $head = $query->execute("#image-main");
            $element = $head->current();
            $info["product_image"] = $element->getAttributeNode("src")->nodeValue;

            $head = $query->execute(".regular-price");
            $elements = $head->current()->getElementsByTagName("span");
            foreach ($elements as $a) {
                $info["product_price"] = $a->nodeValue;
            }
            $head = $query->execute(".product-name");
            $elements = $head->current()->getElementsByTagName("h1");
            foreach ($elements as $a) {
                $info["product_name"] = $a->nodeValue;
            }
            $info["product_url"] = $url;
            $info["product_shop"] = "MyLittleRoom";
        } elseif ($urlParse['host'] == "www.windeln.ch" || $urlParse['host'] == "windeln.ch") {
            $client = new Client($url);
            $response = $client->send();

            $query = new Query($response->getBody());

            $head = $query->execute("html");
            $elements = $head->current()->getElementsByTagName("meta");
            foreach ($elements as $a) {
                $obj = $a->getAttributeNode('property');
                if ($obj) {
                    if($obj->nodeValue == "”og:image”" || $obj->nodeValue == "og:image") {
                        $info['product_image'] = $a->getAttributeNode("content")->nodeValue;
                    }
                }
            }
            $head = $query->execute("#normal-price");
            $element = $head->current();
            $info["product_price"] = $element->nodeValue;
            $head = $query->execute(".product-title");
            $element = $head->current();
            $info["product_name"] = $element->nodeValue;
            $info["product_url"] = $url;
            $info["product_shop"] = "windeln.ch";
        } elseif ($urlParse['host'] == "www.baby-walz.ch" || $urlParse['host'] == "baby-walz.ch") {
            $client = new Client($url);
            $response = $client->send();

            $query = new Query($response->getBody());
            $head = $query->execute(".articleImage");
            $element = $head->current();
            $info["product_image"] = $element->getAttributeNode("src")->nodeValue;

            $head = $query->execute("html");
            $elements = $head->current()->getElementsByTagName("meta");
            foreach ($elements as $a) {
                $obj = $a->getAttributeNode('name');
                if ($obj) {
                    if($obj->nodeValue == "keywords") {
                        $info['product_name'] = $a->getAttributeNode("content")->nodeValue;
                    }
                }
            }
            $head = $query->execute("#productCurrentPrice1_span");
            $element = $head->current();
            $info["product_price"] = $element->nodeValue;
            $info["product_url"] = $url;
            $info["product_shop"] = "BabyWalz";
        } elseif ($urlParse['host'] == "www.toys.ch" || $urlParse['host'] == "toys.ch") {
            $client = new Client($url);
            $response = $client->send();

            $query = new Query($response->getBody());

            $head = $query->execute("html");
            $elements = $head->current()->getElementsByTagName("meta");
            foreach ($elements as $a) {
                $obj = $a->getAttributeNode('property');
                if ($obj) {
                    if($obj->nodeValue == "”og:image”" || $obj->nodeValue == "og:image") {
                        $info['product_image'] = $a->getAttributeNode("content")->nodeValue;
                    }
                }
            }

            $head = $query->execute(".product-title");
            $element = $head->current();
            $info["product_name"] = $element->nodeValue;
            $head = $query->execute(".price-display");
            $element = $head->current();
            $info["product_price"] = trim($element->nodeValue);
            $info["product_url"] = $url;
            $info["product_shop"] = "ToysCh";
        } elseif(strpos($urlParse['host'],"amazon.") !== false) {
            $client = new Client($url);
            $response = $client->send();

            $query = new Query($response->getBody());

            $head = $query->execute("html");
            $elements = $head->current()->getElementsByTagName("meta");
            foreach ($elements as $a) {
                $obj = $a->getAttributeNode('property');
                if ($obj) {
                    if($obj->nodeValue == "”og:image”" || $obj->nodeValue == "og:image") {
                        $info['product_image'] = $a->getAttributeNode("content")->nodeValue;
                    }
                }
            }
            if (!isset($info['product_image'])) {
                if (($head = $query->execute(".image")) && $head->current()) {
                    $elements = $head->current()->getElementsByTagName("img");
                    foreach ($elements as $a) {
                        $obj = $a->getAttributeNode('src');
                        if ($obj) {
                            $info['product_image'] = $obj->nodeValue;
                        }
                    }
                }
            }
            if (!isset($info['product_image'])) {
                if (($head = $query->execute("#main-image-container")) && $head->current()) {
                    $elements = $head->current()->getElementsByTagName("img");
                    foreach ($elements as $a) {
                        $obj = $a->getAttributeNode('src');
                        if ($obj) {
                            $info['product_image'] = $obj->nodeValue;
                        }
                    }
                }
            }

            $head = $query->execute("title");
            $element = $head->current();

            $info["product_url"] = $url;
            $info["product_shop"] = $element->nodeValue;
        } else {
            $client = new Client($url);
            $response = $client->send();

            $query = new Query($response->getBody());

            $head = $query->execute("html");
            $elements = $head->current()->getElementsByTagName("meta");
            foreach ($elements as $a) {
                $obj = $a->getAttributeNode('property');
                if ($obj) {
                    if($obj->nodeValue == "”og:image”" || $obj->nodeValue == "og:image") {
                        $info['product_image'] = $a->getAttributeNode("content")->nodeValue;
                    }
                }
            }

            $head = $query->execute("title");
            $element = $head->current();

            $info["product_url"] = $url;
            $info["product_shop"] = $element->nodeValue;
        }

        if (isset($info) && isset($info['product_image']) && $info['product_image']) {
            $info['product_user_id'] = $this->getUser()->getId();

            $product = $this->getProductTable()->createNew();
            $product->exchangeArray($info);
            $product->setStatus("active");
            $product = $this->getProductTable()->save($product);
        } else {
            return new JsonModel(array(
                'returnCode' => 201,
                'result' => array(
                    "showMessage" => true,
                ),
                "message" => $this->getErrorMsgZendFormat("Sorry! Having trouble to save the link?<br>Add new website to Presont. <a href='javascript:void(0)' data-toggle='modal' data-target='#HelpModalBox' data-dismiss='modal'>Click here</a>.")
            ));
        }

        return new JsonModel(array(
            'returnCode' => 101,
            'result' => array(
                'product' => $product->getArrayCopy(),
                'redirectUrl' => $this->getConfig()->site['domain'],
            ),
        ));
    }

    public function sendContactMessageAction()
    {
        if (!$this->params()->fromPost("contactName", false) || !$this->params()->fromPost("contactEmail", false) ||
            !$this->params()->fromPost("contactSubject", false) || !$this->params()->fromPost("contactMessage", false)
        ) {
            return new JsonModel(array(
                'returnCode' => 101,
                'result' => false,
                'message' => 'Wrong parameters.',
            ));
        }

        $variables = array(
            'request' => $this->params()->fromPost(),
            'domain' => $this->getConfig()->site['domain'],
        );

        $htmlBody = $this->createMailBody('contact-us', $variables);

        $this->sendMail(
            $htmlBody, "[ContactUs] " . $this->params()->fromPost("contactSubject", false),
            $this->params()->fromPost("contactEmail", false), "povtasci@gmail.com");

        return new JsonModel(array(
            'returnCode' => 101,
            'result' => array(
                'contactUsForm' => true,
            ),
        ));
    }

    public function addNewEventAction()
    {
        if (!$this->getUser()->getId()) {
            return new JsonModel(array(
                "returnCode" => 203,
                "message" => "You need to log in.",
            ));
        }
        $form = new \Application\Form\EventForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $event = $this->getEventTable()->createNew();
            $form->setInputFilter($event->getInputFilter());
            $postData = $request->getPost();
            $postData['event_date'] = $postData['event_year'] . "-" . $postData['event_month'] . "-" . $postData['event_day'];
            $form->setData($postData);
            if ($form->isValid()) {
                $event->exchangeArray($form->getData());
                $event->setUserId($this->getUser()->getId());
                $this->getEventTable()->save($event);

                return new JsonModel(array(
                    "returnCode" => 101,
                    "result" => array(
                        "reload" => true,
                    ),
                ));
            } else {
                return new JsonModel(array(
                    "returnCode" => 201,
                    "message" => $form->getMessages(),
                ));
            }
        }
        return new JsonModel(array(
            "returnCode" => 203,
            "message" => $this->getErrorMsgZendFormat("Wrong request."),
        ));
    }

    public function saveEventAction()
    {
        if (!$this->getUser()->getId()) {
            return new JsonModel(array(
                "returnCode" => 203,
                "message" => "You need to log in.",
            ));
        }
        $form = new \Application\Form\EventForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            if (!($event = $this->getEventTable()->find($this->params()->fromPost("event_id"))) || $event->getUserId() != $this->getUser()->getId()) {
                return new JsonModel(array(
                    "returnCode" => 201,
                    "result" => array(
                        "showMessage" => true,
                    ),
                    "message" => $this->getErrorMsgZendFormat("Wrong parameters."),
                ));
            }
            $form->setInputFilter($event->getInputFilter());
            $postData = $request->getPost();
            $postData['event_date'] = $postData['event_year'] . "-" . $postData['event_month'] . "-" . $postData['event_day'];
            $form->setData($postData);
            if ($form->isValid()) {
                $event->exchangeArray($form->getData());
                $event->setUserId($this->getUser()->getId());
                $this->getEventTable()->save($event);

                return new JsonModel(array(
                    "returnCode" => 101,
                    "result" => array(
                        "showMessage" => true,
                    ),
                    "message" => "Event has been saved.",
                ));
            } else {
                return new JsonModel(array(
                    "returnCode" => 201,
                    "message" => $form->getMessages(),
                ));
            }
        }
        return new JsonModel(array(
            "returnCode" => 203,
            "message" => $this->getErrorMsgZendFormat("Wrong request."),
        ));
    }

    public function deleteEventAction()
    {
        if (!$this->getUser()->getId()) {
            return new JsonModel(array(
                "returnCode" => 203,
                "message" => "You need to log in.",
            ));
        }
        if (!($event = $this->getEventTable()->find($this->params()->fromRoute("id"))) || $event->getUserId() != $this->getUser()->getId()) {
            return new JsonModel(array(
                "returnCode" => 201,
                "result" => array(
                    "showMessage" => true,
                ),
                "message" => $this->getErrorMsgZendFormat("Wrong parameters."),
            ));
        }

        $this->getEventTable()->delete($event->getId());

        return new JsonModel(array(
            "returnCode" => 101,
        ));
    }
}
