<?php
namespace Application\Controller;


use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\Mail\Message;
use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Validator\File\MimeType;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Auth\Adapter\AuthAdapter;
use Auth\Model\Auth;
use Auth\Model\AuthTable;
use Auth\Model\AuthStorage;
use Application\Model\ProductTable;
use Application\Model\EventTable;

class RootController extends AbstractActionController
{
    private $authTable;
    private $authAdapter;
    private $authService;
    private $authStorage;
    private $productTable;
    private $eventTable;
    private $user;
    private $config;
    private $lastUrl;

    /**
     * @return AuthTable
     */
    public function getAuthTable()
    {
        if (!$this->authTable) {
            $sm = $this->getServiceLocator();
            $this->authTable = $sm->get('Auth\Model\AuthTable');
        }
        return $this->authTable;
    }

    /**
     * @return ProductTable
     */
    public function getProductTable()
    {
        if (!$this->productTable) {
            $sm = $this->getServiceLocator();
            $this->productTable = $sm->get('Application\Model\ProductTable');
        }
        return $this->productTable;
    }

    /**
     * @return EventTable
     */
    public function getEventTable()
    {
        if (!$this->eventTable) {
            $sm = $this->getServiceLocator();
            $this->eventTable = $sm->get('Application\Model\EventTable');
        }
        return $this->eventTable;
    }

    /**
     * @return AuthAdapter
     */
    public function getAuthAdapter()
    {
        if (!$this->authAdapter) {
            $adapter = new AuthAdapter();
            $this->authAdapter = $adapter->setAuthTable($this->getAuthTable());
        }
        return $this->authAdapter;
    }

    public function getAuthService()
    {
        if (!$this->authService) {
            $this->authService = $this->getServiceLocator()
                ->get('AuthService');
        }

        return $this->authService;
    }

    /**
     * @return AuthStorage
     */
    public function getSessionStorage()
    {
        if (!$this->authStorage) {
            $this->authStorage = $this->getServiceLocator()->get('Auth\Model\AuthStorage');
        }
        return $this->authStorage;
    }

    public function getUser()
    {
        if (!$this->user) {
            if (($this->user = $this->getServiceLocator()->get('AuthService')->getStorage()->read()) &&
                is_a($this->user, "Auth\Model\Auth")
            ) {
            } else {
                $this->user = new Auth();
            }
        }

        return $this->user;
    }

    public function getConfig() {
        if(!$this->config) {
            $config = $this->getServiceLocator()->get('Config');
            $this->config = (object) $config;
        }
        return $this->config;
    }

    public function getLastUrl() {
        if(!$this->lastUrl) {
            $session = new Container('url');
            $this->lastUrl = $session->lastUrl;
            $session->lastUrl = false;
        }
        return $this->lastUrl ? $this->lastUrl : "/";
    }

    public function setLastUrl($url = false) {
        $session = new Container('url');
        if ($url) {
            $session->lastUrl = $url;
        } else {
            $session->lastUrl = $this->getRequest()->getRequestUri();
        }
    }

    public function getErrorMsgZendFormat($msg)
    {
        return array(
            "error_msg" => array(
                $msg,
            ),
        );
    }

    public function logoutUser()
    {
        $this->getSessionStorage()->forgetMe();
        $this->getAuthService()->clearIdentity();
    }

    public function loginUser($user)
    {
        $this->getSessionStorage()
            ->setRememberMe(1);

        $this->getAuthService()->setStorage($this->getSessionStorage());
        $this->getAuthService()->getStorage()->write($user);
        $this->user = null;
    }

    public function reLoginUser()
    {
        $user = $this->getUser(true);
        $this->user = false;
        $this->logoutUser();
        $this->loginUser($user);
        $this->getUser();
    }

    public function sendMail($htmlBody, $subject, $from, $to, $name = false)
    {
        $headers = "From: " . strip_tags($from) . "\r\n";
        $headers .= "Reply-To: ". strip_tags($from) . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        mail($to, $subject, $htmlBody, $headers);
        return true;

        // TODO need to check why doesn't work
        $htmlPart = new MimePart($htmlBody);
        $htmlPart->type = "text/html; charset=UTF-8";

        $body = new MimeMessage();
        $body->setParts(array($htmlPart));

        $message = new Mail\Message();
        $message->setFrom($from);
        $message->addTo($to);
        $message->setSubject($subject);

        $message->setEncoding("UTF-8");
        $message->setBody($body);

        $transport = new SendmailTransport();
        $transport->send($message);
    }

    public function createMailBody($template, $variables)
    {
        $htmlViewPart = new ViewModel();
        $htmlViewPart->setTerminal(true)
            ->setTemplate($template)
            ->setVariables($variables);

        return $this->getServiceLocator()
            ->get('viewrenderer')
            ->render($htmlViewPart);
    }
}
