<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Auth\Controller\Index' => 'Auth\Controller\IndexController',
        ),
    ),

    'router' => array(
        'routes' => array(
            'auth' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/user[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Auth\Controller\index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'fbauth' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/user/fbauth[/:act]',
                    'constraints' => array(
                        'act' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Auth\Controller\index',
                        'action'     => 'fbauth',
                    ),
                ),
            ),
            'resetPassword' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/user/reset-password[/:hash]',
                    'constraints' => array(
                        'hash' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Auth\Controller\index',
                        'action'     => 'resetPassword',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),

        'template_map' => array(
            'forgot-password' => __DIR__ . '/../view/email/forgot-password.phtml',
        ),
    ),
);