<?php
namespace Auth;

use Zend\Db\ResultSet\ResultSet;
use Zend\Authentication\Storage;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\Db\TableGateway\TableGateway;
use Auth\Model\Auth;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function onBootstrap(MvcEvent $e)
    {
        // get user block
        if(!($user = $e->getApplication()->getServiceManager()->get("AuthService")->getStorage()->read())
            || !is_a($user, "Auth\Model\Auth")) {
            $user = new Auth();
        }
        $application = $e->getParam('application');
        $viewModel = $application->getMvcEvent()->getViewModel();

        $this->user = $user;
        $e->setParam("user", $user);
        $viewModel->user = $user;
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Auth\Model\AuthTable' => function ($sm) {
                    $tableGateway = $sm->get('AuthTableGateway');
                    $table = new \Auth\Model\AuthTable($tableGateway);
                    return $table;
                },
                'AuthTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new \Auth\Model\Auth());
                    return new TableGateway('users', $dbAdapter, null, $resultSetPrototype);
                },
                'Auth\Model\AuthStorage' => function ($sm) {
                    return new \Auth\Model\AuthStorage();
                },
                'AuthService' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');

                    $dbTableAuthAdapter = new CredentialTreatmentAdapter($dbAdapter, 'users', 'email', 'password', 'MD5(?)');

                    $authService = new AuthenticationService();
                    $authService->setAdapter($dbTableAuthAdapter);
                    $authService->setStorage($sm->get('Auth\Model\AuthStorage'));

                    return $authService;
                },
            ),
        );
    }

}