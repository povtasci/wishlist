<?php
namespace Auth\Form;

use Zend\Form\Form;

class AuthLoginForm extends Form
{
    public function __construct($name=null)
    {
        parent::__construct('auth');
        $this->setAttribute('method','post');

        $this->add(array(
            'name' => 'user_email',
            'attributes' => array(
                'type'  => 'email',
            ),
        ));

        $this->add(array(
            'name' => 'user_password',
            'attributes' => array(
                'type'  => 'password',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Login',
                'id' => 'submit',
                'class'=>'btn btn-success'
            ),
        ));

    }
}