<?php
namespace Auth\Form;

use Zend\Form\Form;

class ProfileEditForm extends Form
{
    public function __construct($name=null)
    {
        parent::__construct('pofile-edit');
        $this->setAttribute('method','post');

        $this->add(array(
            'name' => 'user_fname',
            'attributes' => array(
                'type'  => 'text',
            ),
        ));

        $this->add(array(
            'name' => 'user_lname',
            'attributes' => array(
                'type'  => 'text',
            ),
        ));

        $this->add(array(
            'name' => 'user_email',
            'attributes' => array(
                'type'  => 'email',
            ),
        ));

        $this->add(array(
            'name' => 'user_country',
            'attributes' => array(
                'type'  => 'email',
            ),
        ));

        $this->add(array(
            'name' => 'user_privacy',
            'attributes' => array(
                'type'  => 'text',
            ),
        ));

    }
}