<?php
namespace Auth\Controller;

use Auth\Model\Auth;
use Application\Controller\RootController;
use Application\Model\Post;
use Application\Model\Banner;
use Application\Model\Media;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter;

use Zend\Http\Client;
use Zend\Session\Container;
use Application\Custom\SimpleImage;
use Application\Custom\HashGenerator;
use Zend\Http\Client as HttpClient;
use Zend\View\View;

class IndexController extends RootController
{

    public function settingsAction()
    {
        if (!$this->getUser()->getId()) {
            return $this->redirect()->toUrl("/");
        }

        $events = $this->getEventTable()->fetchAll(array("event_user_id" => $this->getUser()->getId()));
        $events = $events ? $events['list'] : array();
        $myBirthdayEvent = false;
        foreach ($events as $key => $event) {
            if ($event->getType() == "My Birthday") {
                $myBirthdayEvent = $event;
                unset($events[$key]);
                break;
            }
        }
        return array(
            "events" => $events,
            "myBirthdayEvent" => $myBirthdayEvent,
        );
    }

    /**
     * Facebook OAuth2 callback
     * /fbauth/___/ flow
     * (1) Forward browser to Facebook App Dialog
     * (2) Browser redirected with ?state param
     * (3) cURL for access_token
     * (4) Get Facebook user info through graph.fb.com/me
     * (5) login()/connect()/signup()
     * (6) Display correct page
     */
    public function fbauthAction()
    {
        $request = $this->getRequest()->getQuery()->toArray();

        $auth = array(
            "signup" => $this->params()->fromRoute('act', false) == "signup",
            "login" => $this->params()->fromRoute('act', false) == "login",
            "connect" => $this->params()->fromRoute('act', false) == "connect",
            "state_key" => "fb_ipoints_auth"
        );

        $auth['url'] = $this->getConfig()->site['domain'] . "/user/fbauth/";
        if ($auth['login'])
            $auth['url'] .= "login";
        else if ($auth['connect'])
            $auth['url'] .= "connect";
        else if ($auth['signup'])
            $auth['url'] .= "signup";
        else {
            return $this->redirect()->toRoute("home");
        }
        $fbconfig = array(
            'appId' => $this->getConfig()->FB['api_key'],
            'secret' => $this->getConfig()->FB['app_secret'],
        );
        //var_dump($request);die;
        if (array_key_exists('state', $request) && $request['state']) {
            $state = json_decode(stripslashes(urldecode($request["state"])));
            if (array_key_exists('code', $request) && $request['code']) {
                // User accepted app authorization, retrieve access_token
                $url = "https://graph.facebook.com/oauth/access_token?client_id=" . $fbconfig['appId'] .
                    "&redirect_uri=" . $auth['url'] . "&client_secret=" . $fbconfig['secret'] .
                    "&code=" . $request["code"];

                $config = array(
                    'adapter' => 'Zend\Http\Client\Adapter\Socket',
                    'ssltransport' => 'tls',
                    'sslverifypeer' => false
                );

                $client = new Client($url, $config);
                $response = $client->send();

                if (!$response->isSuccess()) {
                    die($response->getBody());
                } else {
                    try {
                        parse_str($response->getBody(), $facebook_response);
                        $access_token = $facebook_response['access_token'];

                        $config = array(
                            'adapter' => 'Zend\Http\Client\Adapter\Socket',
                            'ssltransport' => 'tls',
                            'sslverifypeer' => false
                        );

                        $client1 = new Client("https://graph.facebook.com/me?access_token=" . $access_token . "&fields=id,first_name,last_name,timezone,email", $config);
                        $response1 = $client1->send();

                        $facebook_user = json_decode($response1->getBody(), true);
                        if ($auth['login']) {
                            $redirect = $this->facebookLogin($state, $facebook_user['id'], $access_token, $facebook_user);
                        } else if ($auth['signup']) {
                            $redirect = $this->facebookSignup($state, $facebook_user['id'], $access_token, $facebook_user);
                        } else if ($auth['connect']) {
                            // @TODO implement
                            $redirect = $this->facebookConnect($state, $facebook_user['id'], $access_token, $facebook_user);
                        }
                    } catch (Exception $e) {
                        //$redirect = $this->getRedirect($state->redirect, $e->getMessage());
                        die("bbbbbbbbbbbbbbbbbbb");
                    }
                }
            } else {
                // User declined the applications Auth Dialog
                return $this->redirect()->toUrl($state->redirect);
            }
        } else {
            $session = new Container('base');
            $session->offsetSet("referrer", $_SERVER['HTTP_REFERER']);
            // the user is calling /fbauth/___/ without any parameters, redirect them to the Facebook login page
            $state = array("state_key" => $auth['state_key']);
            $state["redirect"] = array_key_exists("redirect", $request) ? $request["redirect"] : $this->getConfig()->site['domain'] . "/user/fbauth/";
            $redirect = "https://www.facebook.com/dialog/oauth?client_id=" . $fbconfig['appId'] .
                "&redirect_uri=" . urlencode($auth['url']) .
                "&scope=public_profile,email&state=" . urlencode(json_encode($state));
        }

        if (strpos($redirect, "http") === 0) {
            header("Location: " . $redirect);
        } else {
            if (array_key_exists('state', $request) && $request['state']) {
                $session = new Container('base');

                $referrer = $session->offsetGet("referrer");
                if ($redirect == "/?action=registration") {
                    $referrer .= "?action=registration";
                }
                return $this->redirect()->toUrl($referrer);
            }
            if (!array_key_exists("iframe", $request)) {
                if (!$this->getRequest()->getServer('HTTP_REFERER')) {
                    return $this->redirect()->toUrl("/");
                }
                if (!(($url = $this->getLastUrl()) && strpos($this->getLastUrl(), "dashboard") === false)) {
                    $url = $this->getRequest()->getServer('HTTP_REFERER');
                }
                return $this->redirect()->toUrl($url);
            } else if ($redirect) {
                return $this->redirect()->toUrl($redirect);
            } else {
                if (!(($url = $this->getLastUrl()) && strpos($this->getLastUrl(), "dashboard") === false)) {
                    $url = $this->getRequest()->getServer('HTTP_REFERER');
                }
                return $this->redirect()->toUrl($url);
            }
        }
        exit;
    }// Login Process:
    //	(1) Check for !an existing session
    //	(2) Check for a ShowMe ID attached to this Facebook ID
    //	(3) Connect the ShowMe ID & the Facebook ID (access_token)
    //	(4) Log the user in
    public function facebookLogin($state, $facebook_id, $access_token, $facebook_user)
    {

        if ($this->getUser()->getId()) {
            return $state->redirect;
        } else {
            $user = $this->getAuthTable()->fetchByFbId($facebook_id);

            // user is null try to check is there user wich FB email
            if (!$user) {
                try {
                    $user = $this->getAuthTable()->fetchBy(array("user_email" => $facebook_user['email']));
                    if ($user) {
                        $this->getAuthTable()->connectSocialAccount(
                            $user->getId(), $facebook_id, "facebook", $access_token
                        );
                    }
                } catch (\Exception $e) {
                    $user = false;
                }
            }
            //if user exists then check is inactive otherwise login
            if ($user) {
                // Connect the account so we can retrieve a session
                $this->getAuthTable()->connectSocialAccount(
                    $user->getId(), $facebook_id, "facebook", $access_token
                );
                $user = $this->getAuthTable()->save($user);

                $result = "/";

                if ($user->getStatus() != "active") {
                    $user->setStatus("active");
                    $this->getAuthTable()->save($user);
                }

                $this->loginUser($user);

                return $result;
            } else {
                //if no user and no email then signUp him as new user to iPoints
                return $this->facebookSignup($state, $facebook_id, $access_token, $facebook_user);
            }
        }
    }

    // Signup Process:
    //	(1) Check for !an existing session
    //	(2) Check for !a ShowMe ID attached to this Facebook ID
    //	(3) Create new user
    //	(4) Connect the ShowMe ID & the Facebook ID (access_token, new row, etc.)
    // 	(5) Log the user in
    public function facebookSignup($state, $facebook_id, $access_token, $facebook_user)
    {
        if ($this->getUser()->getId()) {
            return $state->redirect;
        } else {
            $matchingEmail = false;
            $userWithFbId = false;
            try {
                $userWithFbId = $this->getAuthTable()->fetchByFbId($facebook_id);
            } catch (\Exception $e) {
                //ignore any kind of , checking it after
            }
            try {
                $matchingEmail = $this->getAuthTable()->fetchBy(array("user_email" => $facebook_user['email']));
            } catch (\Exception $e) {

            }
            if (!$matchingEmail && !$userWithFbId) {
                $avatar = $this->facebookAvatar($access_token);

                $arg['user_password'] = $facebook_user['id'];
                $arg['user_lname'] = $facebook_user['last_name'];
                $arg['user_fname'] = $facebook_user['first_name'];
                $arg['user_email'] = $facebook_user['email'];
                $arg['user_avatar'] = $avatar;
                $arg['user_fb_id'] = $facebook_user['id'];
                $arg['user_status'] = "active";

                $user = new Auth($arg);

                $user = $this->getAuthTable()->save($user);
                $this->loginUser($user);

                $this->getAuthTable()->connectSocialAccount($user->getId(), $facebook_id, "facebook", $access_token);

                return "/?action=registration";
            } else {
                if (!empty($userWithFbId))
                    $param = "&facebook_id=" . $facebook_id;
                else if (!empty($matchingEmail))
                    $param = "&email=" . urlencode($facebook_user['email']);
                else
                    return;
                return $state->redirect . "?action=registration" . $param;
            }
        }
    }

    public function facebookAvatar($access_token)
    {
        $rand = md5(time());
        $avatarPath = PUBLIC_PATH . "/media/user/fbavatar_" . $rand . ".jpg";
        $avatar = file_get_contents("https://graph.facebook.com/me/picture?type=large&access_token=" . $access_token);
        $fp = fopen($avatarPath, 'w');
        fwrite($fp, $avatar);
        fclose($fp);

        $img = new SimpleImage();
        $img->load($avatarPath);
        $img->thumbnail(250, 250);
        $img->save($avatarPath);
        return "/media/user/fbavatar_" . $rand . ".jpg";
    }

    public function logoutAction()
    {
        if (!$this->getUser()->getId()) {
            return $this->redirect()->toRoute("auth", array("action" => "login"));
        }

        $this->getSessionStorage()->forgetMe();
        $this->getAuthService()->clearIdentity();

        return $this->redirect()->toRoute("home");
    }

    public function loginProcessAction()
    {
        $form = new \Auth\Form\AuthLoginForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $adapter = $this->getAuthAdapter()
                    ->setUsername($request->getPost('user_email'))
                    ->setPassword($request->getPost('user_password'));

                $result = $this->getAuthService()->authenticate($adapter);
                foreach ($result->getMessages() as $message) {
                    $this->flashmessenger()->addMessage($message);
                }
                if ($result->isValid()) {
                    $client = $result->getIdentity();

                    $this->getSessionStorage()->setRememberMe(1);
                    $this->getAuthService()->setStorage($this->getSessionStorage());
                    $this->getAuthService()->getStorage()->write($client);

                    $url = "/";
                    if ($this->getLastUrl()) {
                        $url = $this->getLastUrl();
                    }

                    return new JsonModel(array(
                        "returnCode" => 101,
                        "result" => array(
                            "redirectUrl" => $url,
                        ),
                        "message" => $this->getErrorMsgZendFormat("You have been successfully login."),
                    ));
                } else {
                    return new JsonModel(array(
                        "returnCode" => 201,
                        "message" => $this->getErrorMsgZendFormat("Wrong email or/and password, please try again."),
                    ));
                }
            } else {
                return new JsonModel(array(
                    "returnCode" => 201,
                    "message" => $this->getErrorMsgZendFormat("Wrong email or/and password, please try again."),
                ));
            }
        }
        return new JsonModel(array(
            "returnCode" => 203,
            "message" => $this->getErrorMsgZendFormat("Wrong request."),
        ));
    }

    public function signupProcessAction()
    {
        $form = new \Auth\Form\AuthSignupForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $user = $this->getAuthTable()->createNew();
            $form->setInputFilter($user->getInputFilter());
            $postDate = $request->getPost();
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                if ($this->getAuthTable()->fetchByEmail($data['user_email'])) {
                    return new JsonModel(array(
                        "returnCode" => 201,
                        "message" => $this->getErrorMsgZendFormat("This email already used in our system. Try to use another one."),
                    ));
                }
                $user->exchangeArray($data);
                $user->setPassword(md5($user->getPassword()));
                $user->setStatus('active');

                $user = $this->getAuthTable()->save($user);

                $this->loginUser($user);

                $event = $this->getEventTable()->createNew();
                $event->setName("");
                $event->setType("My Birthday");
                $event->setDate($postDate['user_dob_year'] . "-" . $postDate['user_dob_month'] . "-" . $postDate['user_dob_day']);
                $event->setStatus('public');
                $event->setUserId($user->getId());
                $this->getEventTable()->save($event);

                return new JsonModel(array(
                    "returnCode" => 101,
                    "result" => array(
                        "redirectUrl" => $this->getLastUrl(),
                    ),
                    "message" => $this->getErrorMsgZendFormat("You have been successfully login."),
                ));
            } else {
                return new JsonModel(array(
                    "returnCode" => 201,
                    "message" => $form->getMessages(),
                ));
            }
        }
        return new JsonModel(array(
            "returnCode" => 203,
            "message" => $this->getErrorMsgZendFormat("Wrong request."),
        ));
    }

    public function changeProfileAction()
    {
        if (!$this->getUser()->getId()) {
            return new JsonModel(array("returnCode" => 201, "message" => $this->getErrorMsgZendFormat("You need to log in")));
        }
        $form = new \Auth\Form\ProfileEditForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($this->getUser()->getInputFilterForProfileEdit());
            $form->setData($request->getPost()->toArray());
            if ($form->isValid()) {
                $data = $form->getData();
                if ($this->getUser()->getEmail() != $data['user_email'] && $this->getAuthTable()->fetchByEmail($data['user_email'])) {
                    return new JsonModel(array(
                        "returnCode" => 201,
                        "message" => $this->getErrorMsgZendFormat("This email already used in our system. Try to use another one."),
                    ));
                }
                $this->getUser()->exchangeArray($data);

                $this->getAuthTable()->save($this->getUser());

                if (!($event = $this->getEventTable()->findOne(array('event_user_id' => $this->getUser()->getId(), 'event_type' => "My Birthday")))) {
                    $event = $this->getEventTable()->createNew();
                }
                $postDate = $request->getPost()->toArray();
                $event->setType("My Birthday");
                $event->setDate($postDate['user_dob_year'] . "-" . $postDate['user_dob_month'] . "-" . $postDate['user_dob_day']);
                $event->setStatus($data['user_dob_privacy']);
                $event->setUserId($this->getUser()->getId());
                $this->getEventTable()->save($event);

                $this->reLoginUser();
                return new JsonModel(array(
                    "returnCode" => 101,
                    "result" => array(
                        "showMessage" => $this->getLastUrl(),
                    ),
                    "message" => "Info saved.",
                ));
            } else {
                return new JsonModel(array(
                    "returnCode" => 201,
                    "message" => $form->getMessages(),
                ));
            }
        }
        return new JsonModel(array(
            "returnCode" => 203,
            "message" => $this->getErrorMsgZendFormat("Wrong request."),
        ));
    }

    public function forgotPasswordAction()
    {
        if (!$this->getUser()->getId()) {

            $layout = $this->layout();
            $layout->setTemplate('layout/login');

            return array();
        } else {
            return $this->redirect()->toUrl("/");
        }
    }

    public function resetPasswordAction()
    {
        if (!$this->getUser()->getId()) {
            if (!$this->params()->fromRoute("hash", false) || !$this->getAuthTable()->fetchBy(array("user_hash" => $this->params()->fromRoute("hash", false)))) {
                return $this->redirect()->toUrl("/");
            }

            $layout = $this->layout();
            $layout->setTemplate('layout/login');

            return array(
                "user_hash" => $this->params()->fromRoute("hash", false),
            );
        } else {
            return $this->redirect()->toUrl("/");
        }
    }

    public function sendResetPasswordEmailAction()
    {
        if (!$this->getUser()->getId()) {
            $request = $this->getRequest();
            if ($request->isPost()) {
                if (!$this->params()->fromPost('user_email', false)) {
                    return new JsonModel(array("returnCode" => 201, "message" => $this->getErrorMsgZendFormat("Email is required.")));
                }
                $userFromDb = $this->getAuthTable()->fetchBy(array("user_email" => $this->params()->fromPost('user_email', false)));
                if ($userFromDb){
                    $userFromDb->setHash(HashGenerator::generate());
                    $this->getAuthTable()->save($userFromDb);

                    $variables = array(
                        'user' => $userFromDb,
                        'domain' => $this->getConfig()->site['domain'],
                    );

                    $htmlBody = $this->createMailBody('forgot-password', $variables);

                    $this->sendMail($htmlBody, "Reset Password", 'info@presont.com', $userFromDb->getEmail(), $userFromDb->getFirstName());

                    return new JsonModel(array(
                        "returnCode" => 101,
                        "message" => "Please check your email for a Presont.com reset password.",
                        "result" => array("showMessage" => true),
                    ));
                }
                else {
                    return new JsonModel(array("returnCode" => 201, "message" => $this->getErrorMsgZendFormat("Wrong Email Address.")));
                }
            }
            return new JsonModel(array("returnCode" => 201, "message" => $this->getErrorMsgZendFormat("Wrong Request.")));
        } else {
            return new JsonModel(array("returnCode" => 201, "message" => $this->getErrorMsgZendFormat("Your are logged in")));
        }
    }

    public function resetAction()
    {
        if (!$this->getUser()->getId()) {
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = $request->getPost()->toArray();
                if (strlen($data['user_password']) < 6) {
                    return new JsonModel(array("returnCode" => 201, "message" => $this->getErrorMsgZendFormat("Minimum 6 symbols.")));
                } elseif($data['user_password'] != $data['confirm_password']) {
                    return new JsonModel(array("returnCode" => 201, "message" => $this->getErrorMsgZendFormat("Password doesn't match.")));
                }
                $userFromDb = $this->getAuthTable()->fetchBy(array("user_hash" => $data['user_hash']));
                if ($userFromDb){
                    $userFromDb->setPassword(md5($data['user_password']));
                    $this->getAuthTable()->save($userFromDb);

                    return new JsonModel(array(
                        "returnCode" => 101,
                        "message" => "Password saved",
                        "result" => array("redirectUrl" => "/login"),
                    ));
                }
                else {
                    return new JsonModel(array("returnCode" => 201, "message" => $this->getErrorMsgZendFormat("Security code is not valid")));
                }
            }
            return new JsonModel(array("returnCode" => 201, "message" => $this->getErrorMsgZendFormat("Wrong Request.")));
        } else {
            return new JsonModel(array("returnCode" => 201, "message" => $this->getErrorMsgZendFormat("Your are logged in")));
        }
    }

    public function changePasswordAction()
    {
        if ($this->getUser()->getId()) {
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = $request->getPost()->toArray();
                if ($this->getUser()->getPassword() != md5($data['old_password'])) {
                    return new JsonModel(array("returnCode" => 201, "message" => $this->getErrorMsgZendFormat("Wrong current password.")));
                }
                if (strlen($data['user_password']) < 6) {
                    return new JsonModel(array("returnCode" => 201, "message" => $this->getErrorMsgZendFormat("Minimum 6 symbols.")));
                } elseif($data['user_password'] != $data['confirm_password']) {
                    return new JsonModel(array("returnCode" => 201, "message" => $this->getErrorMsgZendFormat("Password doesn't match.")));
                }
                $this->getUser()->setPassword(md5($data['user_password']));
                $this->getAuthTable()->save($this->getUser());
                $this->reLoginUser();

                return new JsonModel(array(
                    "returnCode" => 101,
                    "message" => "Password saved",
                    "result" => array("showMessage" => true),
                ));
            }
            return new JsonModel(array("returnCode" => 201, "message" => $this->getErrorMsgZendFormat("Wrong Request.")));
        } else {
            return new JsonModel(array("returnCode" => 201, "message" => $this->getErrorMsgZendFormat("You need to log in")));
        }
    }

}