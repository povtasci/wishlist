<?php
namespace Auth\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Auth implements InputFilterAwareInterface
{
    protected $user_id;
    protected $user_fname;
    protected $user_lname;
    protected $user_email;
    protected $user_fb_id;
    protected $user_status;
    protected $user_avatar = '/img/no_avatar.png';
    protected $user_password;
    protected $user_hash;
    protected $user_country;
    protected $user_privacy = 'public';
    protected $user_created_at;
    protected $inputFilter;

    public function __construct(array $data = array())
    {
        $this->exchangeArray($data);
        return $this;
    }

    public function exchangeArray($data)
    {
        foreach ($data as $k => $v) {
            if (property_exists($this, $k)) {
                $this->$k = $v;
            }
        }
    }

    public function getArrayCopy()
    {
        $arr = array(
            'user_fname' => $this->getFirstName(),
            'user_lname' => $this->getLastName(),
            'user_email' => $this->getEmail(),
            'user_fb_id' => $this->getFbId(),
            'user_status' => $this->getStatus(),
            'user_password' => $this->getPassword(),
            'user_avatar' => $this->getAvatar(),
            'user_hash' => $this->getHash(),
            'user_privacy' => $this->getPrivacy(),
            'user_country' => $this->getCountry(),
            'user_created_at' => $this->getCreatedAt(),
        );
        if ($this->getId()) {
            $arr['user_id'] = $this->getId();
        }
        return $arr;
    }

    public function getArrayCopyForInsert()
    {
        $arr = $this->getArrayCopy();
        unset($arr['user_id']);
        unset($arr['user_created_at']);
        return $arr;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name' => 'user_fname',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "First name is required",
                            ),
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'user_lname',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Last name is required",
                            ),
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'user_country',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Country is required",
                            ),
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'user_email',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Email is required",
                            ),
                        ),
                    ),
                    array(
                        'name' => 'EmailAddress',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\EmailAddress::INVALID => "Wrong email address",
                                \Zend\Validator\EmailAddress::INVALID_FORMAT => "Wrong email address",
                                \Zend\Validator\EmailAddress::INVALID_HOSTNAME => "Wrong email address",
                                \Zend\Validator\EmailAddress::INVALID_MX_RECORD => "Wrong email address",
                            ),
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'user_password',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Password is required",
                            ),
                        ),
                    ),
                ),
            )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    public function getInputFilterForProfileEdit()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name' => 'user_fname',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "First name is required",
                            ),
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'user_lname',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Last name is required",
                            ),
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'user_privacy',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Privacy is required",
                            ),
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'user_country',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Country is required",
                            ),
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'user_email',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Email is required",
                            ),
                        ),
                    ),
                    array(
                        'name' => 'EmailAddress',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\EmailAddress::INVALID => "Wrong email address",
                                \Zend\Validator\EmailAddress::INVALID_FORMAT => "Wrong email address",
                                \Zend\Validator\EmailAddress::INVALID_HOSTNAME => "Wrong email address",
                                \Zend\Validator\EmailAddress::INVALID_MX_RECORD => "Wrong email address",
                            ),
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'user_dob_privacy',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    public function getId()
    {
        return $this->user_id;
    }

    public function setId($id)
    {
        $this->user_id = $id;
        return $this;
    }

    public function getFirstName()
    {
        return $this->user_fname;
    }

    public function setFirstName($fname)
    {
        $this->user_fname = $fname;
        return $this;
    }

    public function getLastName()
    {
        return $this->user_lname;
    }

    public function setLastName($lname)
    {
        $this->user_lname = $lname;
        return $this;
    }

    public function getEmail()
    {
        return $this->user_email;
    }

    public function setEmail($email)
    {
        $this->user_email = $email;
        return $this;
    }

    public function getFbId()
    {
        return $this->user_fb_id;
    }

    public function setFbId($fbId)
    {
        $this->user_fb_id = $fbId;
        return $this;
    }

    public function getPassword()
    {
        return $this->user_password;
    }

    public function setPassword($password)
    {
        $this->user_password = $password;
        return $this;
    }

    public function getStatus()
    {
        return $this->user_status;
    }

    public function setStatus($status)
    {
        $this->user_status = $status;
        return $this;
    }

    public function getAvatar()
    {
        return $this->user_avatar;
    }

    public function setAvatar($avatar)
    {
        $this->user_avatar = $avatar;
        return $this;
    }

    public function getHash()
    {
        return $this->user_hash;
    }

    public function setHash($hash)
    {
        $this->user_hash = $hash;
        return $this;
    }

    public function getPrivacy()
    {
        return $this->user_privacy;
    }

    public function setPrivacy($privacy)
    {
        $this->user_privacy = $privacy;
        return $this;
    }

    public function getCountry()
    {
        return $this->user_country;
    }

    public function setCountry($country)
    {
        $this->user_country = $country;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->user_created_at;
    }

    public function setCreatedAt($created_at)
    {
        $this->user_created_at = $created_at;
        return $this;
    }

    public function getFullName()
    {
        return $this->getFirstName() . " " . $this->getLastName();
    }
}