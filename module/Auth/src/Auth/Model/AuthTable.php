<?php
namespace Auth\Model;

use Zend\Authentication\Result;
use Zend\Db\TableGateway\TableGateway;
use Auth\Model\Auth;
use Zend\Db\Adapter\Adapter;

class AuthTable
{
    protected $tableGateway;
    protected $adapter;
    protected $_primary = "user_id";

    public function createNew()
    {
        return new Auth();
    }

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter = new Adapter($this->tableGateway->getAdapter()->getDriver());
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function save(Auth $adminAuth)
    {
        $data = $adminAuth->getArrayCopyForInsert();
        $id = (int)$adminAuth->getId();
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $res = $this->tableGateway->select(array($this->_primary => $this->tableGateway->lastInsertValue))->current();
        } else {
            if ($this->fetchById($id)) {
                $this->tableGateway->update($data, array($this->_primary => $id));
                $res = $adminAuth;
            } else {
                throw new \Exception('Form id does not exist');
            }
        }

        return $res;
    }

    public function fetchByEmail($email)
    {
        $resultSet = $this->tableGateway->select(array('user_email' => $email));
        $column = $resultSet->current();
        return $column;
    }

    public function fetchBy($args)
    {
        $resultSet = $this->tableGateway->select($args);
        $column = $resultSet->current();
        return $column;
    }

    public function fetchById($id)
    {
        $resultSet = $this->tableGateway->select(array('user_id' => $id));
        $column = $resultSet->current();
        return $column;
    }

    public function fetchByFbId($id)
    {
        $resultSet = $this->tableGateway->select(array('user_fb_id' => $id));
        $column = $resultSet->current();
        return $column;
    }

    public function search($keyword)
    {
        $sql = "SELECT * FROM users WHERE CONCAT_WS(' ', user_fname, user_lname) like '%" . addslashes($keyword) . "%' AND user_privacy = 'public' ";
        $users = array();
        if ($res = $this->adapter->query($sql)->execute()) {
            while ($row = $res->current()) {
                $users[] = new Auth($row);
                $res->next();
            }
        }

        return $users;
    }

    public function authenticate($username, $password)
    {
        $resultSet = $this->tableGateway->select(array('user_email' => $username, 'user_password' => md5($password)));
        $code = ($res = $resultSet->current()) ? 1 : -1;
        $res = new Result($code, $res);
        return $res;
    }

    public function connectSocialAccount($userId, $accountId, $accountName, $accessToken = false,
                                         $accessTokenSecret = false, $expirationDate = false)
    {
        $sql = "SELECT * FROM rel_social_accounts WHERE account_name = '" . $accountName . "' AND account_id = " . $accountId;
        $res = $this->adapter->query($sql)->execute();
        if ($res->count() == 0) {
            $sql = "INSERT INTO rel_social_accounts
                        SET user_id = " . $userId . ", account_id = " . $accountId . ", account_name = '" . $accountName . "'";
            if ($accessToken != false)
                $sql .= ", access_token = '" . $accessToken . "'";
            if ($accessTokenSecret != false)
                $sql .= ", access_token_secret = '" . $accessTokenSecret . "'";
            if ($expirationDate != false)
                $sql .= ", expiration_date = '" . $expirationDate . "'";
            $r = $this->adapter->query($sql)->execute();

            if (!$r) {
                return false;
            }
            return true;
        } else {
            $socialAccount = $res->current();
            if ($userId != $socialAccount["user_id"]) {
                return false;
            }

            $sql = "UPDATE rel_social_accounts SET access_token = ";
            $sql .= ($accessToken == false) ? "NULL" : "'" . $accessToken . "'";
            $sql .= ", access_token_secret = ";
            $sql .= ($accessTokenSecret == false) ? "NULL" : "'" . $accessTokenSecret . "'";
            if ($expirationDate != false) {
                $sql .= ", expiration_date = '" . $expirationDate . "' ";
            }
            $sql .= ", user_id = " . $userId . " WHERE account_id = " . $accountId;
            $r = $this->adapter->query($sql)->execute();
            if (!$r) {
                return false;
            }

            return true;
        }
    }
}