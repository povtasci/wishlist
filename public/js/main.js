$(document).ready(function() {
    $(".modal-backdrop, #videoModal .close, #videoModal .btn").on("click", function() {
        $("#videoModal iframe").attr("src", $("#videoModal iframe").attr("src"));
    });

    $("#content").css("min-height", (screen.height - 350) + "px");
    $(".ajaxForm").ajaxForm(function (e, statusText, xhr, $form) {
        if (e.returnCode == 201) {
            var error_msg;
            $.each(e.message, function (index, value) {
                $.each(value, function (i, v) {
                    $('#' + index + '_div input').addClass('inp_error');

                    if (error_msg) {
                        error_msg = error_msg + ",<br>" + v;
                    } else {
                        error_msg = v;
                    }
                });
            });
            showDangerAlert(error_msg);
        } else if (e.returnCode == 101) {
            showInfoAlert(e.message);
            if (e.result && e.result.redirectUrl) {
                window.location.replace(e.result.redirectUrl);
            } else if (e.result && e.result.reload) {
                window.location.reload();
            } else if (e.result && e.result.showMessage) {
                showInfoAlert(e.message);
            } else if (e.result && e.result.contactUsForm) {
                $form.addClass("hide");
                $(".modal-body.hide .alert.alert-success").removeClass("hide");
                $(".modal-body.hide").removeClass("hide");
                $(".modal-footer.hide").removeClass("hide");
            }
        } else if (e.returnCode == 203) {
            $('div.accept_checkbox div.checker span').addClass('error_check');
            showDangerAlert(e.message);
        }
        return false;
    });
    $(".ajaxFormSettings").ajaxForm(function (e, statusText, xhr, $form) {
        if ($form.find(".alert").length) {
            $form.find(".alert").addClass("hide");
        }
        if (e.returnCode > 200) {
            var error_msg;
            $.each(e.message, function (index, value) {
                $.each(value, function (i, v) {
                    $('#' + index + '_div input').addClass('inp_error');

                    if (error_msg) {
                        error_msg = error_msg + ",<br>" + v;
                    } else {
                        error_msg = v;
                    }
                });
            });
            $form.find(".errorAlert").html(error_msg);
            $form.find(".errorAlert").removeClass("hide");
        } else if (e.returnCode == 101) {
            if (e.result && e.result.showMessage) {
                $form.find(".infoAlert").html(e.message);
                $form.find(".infoAlert").removeClass("hide");
            } else if (e.result && e.result.reload) {
                window.location.reload();
            } else if (e.result && e.result.redirectUrl) {
                window.location.replace(e.result.redirectUrl);
            }
        }
        return false;
    });

    setTimeout(function() {
        $(".share-buttons").animate({"left" : "4px"}, 500);
        $(".leftColumn").animate({"margin-left" : "0px"}, 100);
        $(".column .logo").animate({"top" : "40px"}, 100);
    }, 100);

    $('.datePickerInput').datepicker({
        format: "yyyy-mm-dd",
        startView: 2
    });
    $('.tooltipElement').tooltip();

    //$("." + mansoryContainerName).masonry({
    //    // options
    //    itemSelector: '.pin',
    //    isAnimated: true,
    //    isFitWidth: true
    //});
});

var showDangerAlert = function(msg) {
    $(".alert").addClass("hide");
    $(".errorAlert").html(msg);
    $(".errorAlert").removeClass("hide");
}

var showInfoAlert = function(msg) {
    if ($(".alert").length) {
        $(".alert").addClass("hide");
    }
    if ($(".infoAlert").length) {
        $(".infoAlert").html(msg);
        $(".infoAlert").removeClass("hide");
    }
}

var shareLinkToFB = function(link) {
    FB.ui({
        method: 'share',
        href: link,
        app_id: 1156810517671802
    }, function(response){console.log(response)});
}

$(document).ready(function(){
    if ($("#userCountrySelect").length) {
        $("#userCountrySelect").val($("#userCountrySelect").data('country'));
    }

    var submitIcon = $('.searchbox-icon');
    var inputBox = $('.searchbox-input');
    var searchBox = $('.searchbox');
    var isOpen = false;
    submitIcon.click(function(){
        if(isOpen == false){
            searchBox.addClass('searchbox-open');
            setTimeout(function(){inputBox.focus()}, 500);
            isOpen = true;
        } else {
            searchBox.removeClass('searchbox-open');
            isOpen = false;
        }
    });
    submitIcon.mouseup(function(){
        return false;
    });
    searchBox.mouseup(function(){
        return false;
    });
    $(document).mouseup(function(){
        if(isOpen == true){
            $('.searchbox-icon').css('display','block');
            submitIcon.click();
        }
    });

    $('.imageUploadInput').change(function () {
        var form = $(this).closest(".imageUploadForm");
        $(form).ajaxForm({
            success: function (res) {
                if (res.returnCode == 101) {
                    $(".img-responsive.img-thumbnail").attr("src", res.result.url);
                } else {
                    showDangerAlert(res.msg);
                }
            }
        }).submit();
    });
    $('#addNewPresnetModalBox').on('shown.bs.modal', function (e) {
        $('#url').focus();
    })
});
function buttonUp(){
    var searchBox = $('.searchbox');
    if(searchBox.hasClass('searchbox-open')){
        $('.searchbox-icon').css('z-index',-1);
    } else {
        $('.searchbox-icon').css('z-index',1);
    }
}
var ADProductId;
var ADel;
var ADAction;
function moveToArchiveModal(productId, action, text, el) {
    ADProductId = productId;
    ADel = el;
    ADAction = action;
    if (action == 'archive' && !$(el).data('sameUser')) {
        $("#confirmASModal .modal-footer a").text('Yes, archive it!');
        $("#confirmASModal .modal-footer button").text('No, not now!');
    } else {
        $("#confirmASModal .modal-footer a").text('Yes');
        $("#confirmASModal .modal-footer button").text('No');
    }
    $("#confirmASModal .modal-body h4").text(text);
    $("#confirmASModal").modal('show');

}
function moveToAD() {
    $("#confirmASModal").modal('hide');
    if (ADAction == 'archive') {
        $.post("/action/move-to-archive/" + ADProductId, function(res) {
            if (res.returnCode == 101) {
                $(ADel).closest(".col-md-20").fadeOut(900);
                setTimeout(function() {
                    var $container = $('.container-gallery');
                    $container.imagesLoaded( function(){
                        $container.masonry();
                    });
                }, 1000);
            } else {
                alert(res.message);
            }
        })
    } else if (ADAction == 'delete') {
        $.post("/action/delete-product/" + ADProductId, function(res) {
            if (res.returnCode == 101) {
                $(ADel).closest(".col-md-20").fadeOut(900);
                setTimeout(function() {
                    var $container = $('.container-gallery');
                    $container.imagesLoaded( function(){
                        $container.masonry();
                    });
                }, 1000);
            } else {
                alert(res.message);
            }
        })

    } else {
        $.post("/action/move-to-list/" + ADProductId, function(res) {
            if (res.returnCode == 101) {
                $(ADel).closest(".col-md-20").fadeOut(900);
                setTimeout(function() {
                    var $container = $('.container-gallery');
                    $container.imagesLoaded( function(){
                        $container.masonry();
                    });
                }, 1000);
            } else {
                alert(res.message);
            }
        })

    }
    return false;
}

function showAddNewEventBox() {
    $(".showNewEventBox").closest(".post-container").addClass("hide");
    $(".newEventBox").removeClass("hide");
}

function hideAddNewEventBox() {
    $(".newEventBox").addClass("hide");
    $(".showNewEventBox").closest(".post-container").removeClass("hide");
}

function removeMyEvent(id) {
    if (confirm("Are you sure want to delete this event ?")) {
        $.post("/action/delete-event/" + id, function(res) {
            if (res.returnCode == 101) {
                window.location.reload();
            }
        });
    }
}